#!/bin/bash
hoy=`date -R`
docHOSTNAME=`hostname`
docMIIP=`hostname -I`
#informacion sistema operativo
docUNAME=`uname -a`
docISSUE=`cat /etc/issue`
docRELEASE="undef"
if [ -f "/etc/redhat-release" ];
then
    docOSBASE="REDHAT"
    docRELEASE=`cat /etc/redhat-release`
    docOS=`cat /etc/redhat-release`
else
    docOSBASE="DEBIAN"
    docRELEASE=`cat /etc/debian_version`
    docOS=`head -n 1 /etc/issue`
fi

# Cabecera
#echo "<!-- TITLE: $docHOSTNAME -->"
#echo "<!-- SUBTITLE: Servidor $docHOSTNAME - UHAC -->"
echo "[TOC]"

#Informacionb Discos
docFSTAB=`cat /etc/fstab`
docLSBLK=`lsblk`
docFDISK=`fdisk -l`
docDF=`df -h`
docSWAP=`cat /proc/swaps`

#Informacion de redes
docHOSTS=`cat /etc/hosts`
docRESOLV=`cat /etc/resolv.conf`

printf "\n"
echo "# Información General"
printf "\n"
printf "| Hostname | %s | \n" "$docHOSTNAME"
printf "|----------|------------|\n"
printf "| Sistema Operativo | %s - %s | \n" "$docOS" "$docRELEASE"
printf "| Linux Version | %s\n" "$docUNAME |"
printf "| IP | %s |\n" "$docMIIP"
printf "| Servicios | |\n"
printf "| Última Actualización | %s | \n" "$hoy"
printf "\n"
echo "# Información de Hardware"
printf "\n"
echo "## Discos Duros"
printf "\n"
echo "\`\`\`"
printf "%s\n" "$docLSBLK"
echo "\`\`\`"
printf "\n"
echo "### Output de FDISK"
printf "\n"
echo "\`\`\`"
printf "%s\n" "$docFDISK"
echo "\`\`\`"
printf "\n"
echo "### SWAP"
printf "\n"
echo "\`\`\`"
printf "\n"
printf "%s\n" "$docSWAP"
printf "\n"
echo "\`\`\`"
printf "\n"
echo "### Espacio Utilizado"
printf "\n"
echo "\`\`\`"
printf "%s\n" "$docDF"
echo "\`\`\`"
printf "\n"
echo "### Archivo /etc/fstab"
printf "\n"
echo "\`\`\`"
printf "%s\n" "$docFSTAB"
echo "\`\`\`"
printf "\n"

if [ -f "/sbin/ip" ];
then
    docNETDEV=`ip addr show`
elif [ -f "/sbin/ifconfig" ];
then
    docNETDEV=`ifconfig -a`
else
    docNETDEV="IP and IFCONFIG NOT FOUND! Check ASAP!"
fi

echo "## Configuración de red"
printf "\n"
echo "### Información de dispositivos de red"
printf "\n"
echo "Interfaces de red activas"
printf "\n"
echo "\`\`\`"
printf "%s\n" "$docNETDEV"
echo "\`\`\`"
printf "\n"

if [ -f "/sbin/ip" ];
then
	echo "Salida de comando *ip route show*"
	printf "\n"
	echo "\`\`\`"
	ip route show
	echo "\`\`\`"
else
	echo "Salida de comando *route -n*"
	printf "\n"
	echo "\`\`\`"
	route -n
	echo "\`\`\`"
fi

printf "\n"

if [ $docOSBASE = "REDHAT" ];
then
    for i in `ls /etc/sysconfig/network-scripts/ifcfg-*`;
    do
    echo "### Archivo $i"
    printf "\n"
    echo "\`\`\`"
    cat $i
    echo "\`\`\`"
    done
elif [ $docOSBASE = "DEBIAN" ]
then
    echo "### Archivo /etc/network/interfaces"
    printf "\n"
    echo "\`\`\`"
    cat /etc/network/interfaces
    echo "\`\`\`"
else
    echo "**No se han encontrado archivos de configuración**"
    echo "CHECK INTERFACE CONFIG FILES"
    printf "\n"

fi
printf "\n"
# informacion servicios en ejecucion
if [ -f "/usr/bin/nmap" ];
then
    docESCUCHA=`nmap -sT -O localhost`
    docNMAP="nmap found!"
elif [ -f "/sbin/ss" ];
then
    docESCUCHA=`ss -ltp`
    docNMAP="Try: nmap -sT -O $docMIIP"
else
    docESCUCHA=`netstat -ltp`
    docNMAP="Try: nmap -sT -O $docMIIP"
fi

echo "# Servicios en ejecución y puertos escuchando"
printf "\n"
echo "\`\`\`"
printf "%s\n" "$docESCUCHA"
echo "\`\`\`"
printf "\n"
printf "%s\n" "$docNMAP"

printf "\n"
echo "# Servicio XXXX Archivos de configuración"
printf "\n"
echo "ADD SPECIFIC INFORMATION AS REQUIRED"
printf "\n"
echo "# Usuarios registrados"
printf "\n"
echo "\`\`\`"
cat /etc/passwd | grep  home | grep -v false | cut -d : -f 1
echo "\`\`\`"
printf "\n"
echo "## Usuarios en /etc/passwd con carpeta bajo /var"
printf "\n"
echo "\`\`\`"
cat /etc/passwd | grep var | grep -E "www|lib" | cut -d : -f 1
echo "\`\`\`"
printf "\n"
echo "# Registros miscelaneos"
printf "\n"
echo "Agrega información de registro o situaciones que pueden ser importantes!"
printf "\n"
echo "# Paquetes de sistema"
printf "\n"
if [ $docOSBASE = "REDHAT" ];
then
    if [ -f /usr/bin/dnf ];
    then
        echo "## Historial de paquetes"
        printf "\n"
        echo "\`\`\`"
        dnf history all
        echo "\`\`\`"
        printf "\n"
        echo "## Paquetes instalados"
        printf "\n"
        echo "\`\`\`"
        dnf list installed | awk '{print $1}'
        echo "\`\`\`"
    else
        echo "## Historial de paquetes"
        printf "\n"
        echo "\`\`\`"
        yum history
        echo "\`\`\`"
        printf "\n"
        echo "## Paquetes instalados"
        printf "\n"
        echo "\`\`\`"
        yum list installed | grep -v ^lib | awk '{print $1}'
        echo "\`\`\`"
    fi
else
    echo "\`\`\`"
    if [ -f "/usr/bin/apt" ];
    then
        apt list --installed | grep -v ,automatic | grep -v ^lib | awk -F'/' '{print $1}'
    else

        dpkg-query -l | awk '{print $2}'
    fi
    echo "\`\`\`"
fi
printf "\n"
