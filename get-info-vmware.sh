# This scipt has been writen to get information from ESXI 6

echo '[TOC]'
echo

# Host information
# hostname
getHOSTNAME=`uname -n`

# OS
# getOS=`uname -o`

# Kernel release
# getKERNEL=`uname -r`
# getKERNELNAME=`uname -s`

printf "# Host Information\n"
printf "\n"
printf "| Hostname | %s | \n" "$getHOSTNAME"
printf "|----------|------------------| \n"
# printf "| OS | %s | \n" "$getOS"
# printf "| Kernel | %s - %s | \n" "$getKERNELNAME" "$getKERNEL"
esxcli system version get | grep -E "Product|Version|Build" | awk -F ':' '{print "\x7c "$1  " \x7c" $2 " \x7c"}'
esxcli hardware platform get | grep -E  "Product|Serial" | awk -F ':' '{print "\x7c "$1  " \x7c" $2 " \x7c"}'
echo
echo

# Disk information
printf "# Disk Information \n"
echo

printf "## \`df\` Output \n"
echo '```'
df -h
echo '```'
echo

printf "## \`esxcli storage extent list\` Output \n"
echo '```'
esxcli storage vmfs extent list
echo '```'
echo


# Get IPv4 information
printf "# Network Information \n"
echo

printf "## IP and interfaces \n"
echo '```'
esxcli network ip interface ipv4 get
echo '```'
echo

# Get network interface information
printf "## Interfaces information \n"
echo '```'
esxcli network ip interface list
echo '```'
echo

# Get Neighbor
printf "## Neighbor list \n"
echo '```'
esxcli network ip neighbor list
echo '```'
echo

# Virtual Machines Information
printf "# Virtual Machines Hosted \n"
printf "## Virtual Machine list \n"

# Put a list here.
# List virtual machines in Vsphere
# Export the list to CSV, edit the list and add it!

echo

printf "## \`esxcli vm process list\` Output \n"
echo
echo "TL;DR"
echo
echo '```'
esxcli vm process list
echo '```'
echo
# Other commands that you can use to get information.
# esxcli hardware platform get
# esxcli system version get
# esxcli storage vmfs extent list
# esxcli storage filesystem list
# esxcli system account list
# esxcli vm process list
