# README #

This is a script that I wrote to get a quick summary from Linux machines to get a quick overview of the systems. Also there is one to use with ESXI command line.

Run it, copy the output, drop it.

### What is this repository for? ###

This is a simple script made to get information from your system in Markdown format.

Then, you can copy the output in a wiki and get an overview from your system:

 * OS Version
 * Filesystem information
 * Network configuration
 * Running Services
 * Users under /home and /var
 * Cron tasks
 * Packages installed

### How do I get set up? ###

To use this script, download it and run it as follow:

`sudo bash script > output.md`

Then you can copy the output file or the content in a wiki if you want.

### TODO ###

 * Make it safer
 * Add Options support to make it smarter
 * Suggestions? Drop me a line :)
